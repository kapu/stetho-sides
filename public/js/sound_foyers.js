function stenosis(){
  document.getElementById('thorax').getSVGDocument().getElementById("Foyer_Aortique").setAttribute("style", "opacity:1");
  document.getElementById('thorax').getSVGDocument().getElementById("foyerAortique").setAttribute("src", "assets/sounds/Bruits_du_coeur_test_stenose.ogg");
  document.getElementById('thorax').getSVGDocument().getElementById("foyerAortique").setAttribute("type", "audio/mp3");

    console.log("Sténose");
}

function normal(){
  document.getElementById('thorax').getSVGDocument().getElementById("Foyer_Aortique").setAttribute("style", "opacity:0");
  document.getElementById('thorax').getSVGDocument().getElementById("foyerAortique").setAttribute("src", "assets/sounds/Bruits_du_coeur_test_sans_bruit.ogg");
  document.getElementById('thorax').getSVGDocument().getElementById("foyerAortique").setAttribute("type", "audio/ogg");
  console.log("Normal");
}
